<HTML><HEAD><TITLE>Manpage of FSINFO</TITLE>
</HEAD><BODY>
<H1>FSINFO</H1>
Section: Linux Programmer's Manual (2)<BR>Updated: 2018-06-06<BR><A HREF="#index">Index</A>
<A HREF="/man/man2html">Return to Main Contents</A><HR>

<A NAME="lbAB">&nbsp;</A>
<H2>NAME</H2>

fsinfo - Get filesystem information
<A NAME="lbAC">&nbsp;</A>
<H2>SYNOPSIS</H2>

<PRE>
<B>#include &lt;<A HREF="file:///usr/include/sys/types.h">sys/types.h</A>&gt;</B>
<B>#include &lt;<A HREF="file:///usr/include/sys/fsinfo.h">sys/fsinfo.h</A>&gt;</B>
<B>#include &lt;<A HREF="file:///usr/include/unistd.h">unistd.h</A>&gt;</B>
<B>#include &lt;<A HREF="file:///usr/include/fcntl.h">fcntl.h</A>&gt;           </B>/* Definition of AT_* constants */

<B>int fsinfo(int </B><I>dirfd</I><B>, const char *</B><I>pathname</I><B>,</B>
<B>           struct fsinfo_params *</B><I>params</I><B>,</B>
<B>           void *</B><I>buffer</I><B>, size_t </B><I>buf_size</I><B>);</B>
</PRE>

<P>

<I>Note</I>:

There is no glibc wrapper for
<B>fsinfo</B>();

see NOTES.
<A NAME="lbAD">&nbsp;</A>
<H2>DESCRIPTION</H2>

<P>

fsinfo() retrieves the desired filesystem attribute, as selected by the
parameters pointed to by
<I>params</I>,

and stores its value in the buffer pointed to by
<I>buffer</I>.

<P>

The parameter structure is optional, defaulting to all the parameters being 0
if the pointer is NULL.  The structure looks like the following:
<P>


<PRE>
struct fsinfo_params {
    __u32 at_flags;     /* AT_SYMLINK_NOFOLLOW and similar flags */
    __u32 request;      /* Requested attribute */
    __u32 Nth;          /* Instance of attribute */
    __u32 Mth;          /* Subinstance of Nth instance */
    __u32 __reserved[6]; /* Reserved params; all must be 0 */
};
</PRE>


<P>

The filesystem to be queried is looked up using a combination of
<I>dfd</I>, <I>pathname</I> and <I>params-&gt;at_flags.</I>

This is discussed in more detail below.
<P>

The desired attribute is indicated by
<I>params-&gt;request</I>.

If
<I>params</I>

is NULL, this will default to
<B>fsinfo_attr_statfs</B>,

which retrieves some of the information returned by
<B>statfs</B>().

The available attributes are described below in the &quot;THE ATTRIBUTES&quot; section.
<P>

Some attributes can have multiple values and some can even have multiple
instances with multiple values.  For example, a network filesystem might use
multiple servers.  The names of each of these servers can be retrieved by
using
<I>params-&gt;Nth</I>

to iterate through all the instances until error
<B>ENODATA</B>

occurs, indicating the end of the list.  Further, each server might have
multiple addresses available; these can be enumerated using
<I>params-&gt;Nth</I>

to iterate the servers and
<I>params-&gt;Mth</I>

to iterate the addresses of the Nth server.
<P>

The amount of data written into the buffer depends on the attribute selected.
Some attributes return variable-length strings and some return fixed-size
structures.  If either
<I>buffer</I> is  NULL  or <I>buf_size</I> is 0

then the size of the attribute value will be returned and nothing will be
written into the buffer.
<P>

The
<I>params-&gt;__reserved</I>

parameters must all be 0.

<A NAME="lbAE">&nbsp;</A>
<H3>Allowance for Future Attribute Expansion</H3>

<P>

To allow for the future expansion and addition of fields to any fixed-size
structure attribute,
<B>fsinfo</B>()

makes the following guarantees:
<OL>
  <LI>It will always clear any excess space in the buffer.
  <LI>It will always return the actual size of the data.
  <LI>It will truncate the data to fit it into the buffer rather than giving an
    error.
  <LI>Any new version of a structure will incorporate all the fields from the old
    version at same offsets.
</OL>

<P>

So, for example, if the caller is running on an older version of the kernel
with an older, smaller version of the structure than was asked for, the kernel
will write the smaller version into the buffer and will clear the remainder of
the buffer to make sure any additional fields are set to 0.  The function will
return the actual size of the data.
<P>

On the other hand, if the caller is running on a newer version of the kernel
with a newer version of the structure that is larger than the buffer, the write
to the buffer will be truncated to fit as necessary and the actual size of the
data will be returned.
<P>

Note that this doesn't apply to variable-length string attributes.
<P>

<A NAME="lbAF">&nbsp;</A>
<H3>Invoking <B>fsinfo</B>():</H3>

<P>

To access a file's status, no permissions are required on the file itself, but
in the case of
<B>fsinfo</B>()

with a path, execute (search) permission is required on all of the directories
in
<I>pathname</I>

that lead to the file.
<P>

<B>fsinfo</B>()

uses
<I>pathname</I>, <I>dirfd</I> and <I>params-&gt;at_flags</I>

to locate the target file in one of a variety of ways:
<DL COMPACT>
<DT>[*] By absolute path.<DD>
<I>pathname</I>

points to an absolute path and
<I>dirfd</I>

is ignored.  The file is looked up by name, starting from the root of the
filesystem as seen by the calling process.
<DT>[*] By cwd-relative path.<DD>
<I>pathname</I>

points to a relative path and
<I>dirfd</I> is <I>AT_FDCWD</I>.

The file is looked up by name, starting from the current working directory.
<DT>[*] By dir-relative path.<DD>
<I>pathname</I>

points to relative path and
<I>dirfd</I>

indicates a file descriptor pointing to a directory.  The file is looked up by
name, starting from the directory specified by
<I>dirfd</I>.

<DT>[*] By file descriptor.<DD>
<I>pathname</I> is <I>NULL</I> and <I>dirfd</I>

indicates a file descriptor.  The file attached to the file descriptor is
queried directly.  The file descriptor may point to any type of file, not just
a directory.
</DL>
<P>

<I>flags</I>

can be used to influence a path-based lookup.  A value for
<I>flags</I>

is constructed by OR'ing together zero or more of the following constants:
<DL COMPACT>
<DT><B>AT_EMPTY_PATH</B>

<DD>

If
<I>pathname</I>

is an empty string, operate on the file referred to by
<I>dirfd</I>

(which may have been obtained using the
<B><A HREF="/man/man2html?2+open">open</A></B>(2)

<B>O_PATH</B>

flag).
If
<I>dirfd</I>

is
<B>AT_FDCWD</B>,

the call operates on the current working directory.
In this case,
<I>dirfd</I>

can refer to any type of file, not just a directory.
This flag is Linux-specific; define
<B>_GNU_SOURCE</B>


to obtain its definition.
<DT><B>AT_NO_AUTOMOUNT</B>

<DD>
Don't automount the terminal (&quot;basename&quot;) component of
<I>pathname</I>

if it is a directory that is an automount point.  This allows the caller to
gather attributes of the filesystem holding an automount point (rather than
the filesystem it would mount).  This flag can be used in tools that scan
directories to prevent mass-automounting of a directory of automount points.
The
<B>AT_NO_AUTOMOUNT</B>

flag has no effect if the mount point has already been mounted over.
This flag is Linux-specific; define
<B>_GNU_SOURCE</B>


to obtain its definition.
<DT><B>AT_SYMLINK_NOFOLLOW</B>

<DD>
If
<I>pathname</I>

is a symbolic link, do not dereference it:
instead return information about the link itself, like
<B>lstat</B>().

</DL>
<A NAME="lbAG">&nbsp;</A>
<H2>THE ATTRIBUTES</H2>

<P>

There is a range of attributes that can be selected from.  These are:
<P>

<DL COMPACT>
<DT><B>fsinfo_attr_statfs</B>

<DD>
This retrieves the &quot;dynamic&quot;
<B>statfs</B>

information, such as block and file counts, that are expected to change whilst
a filesystem is being used.  This fills in the following structure:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_statfs {
    __u64 f_blocks;     /* Total number of blocks in fs */
    __u64 f_bfree;      /* Total number of free blocks */
    __u64 f_bavail;     /* Number of free blocks available to ordinary user */
    __u64 f_files;      /* Total number of file nodes in fs */
    __u64 f_ffree;      /* Number of free file nodes */
    __u64 f_favail;     /* Number of free file nodes available to ordinary user */
    __u32 f_bsize;      /* Optimal block size */
    __u32 f_frsize;     /* Fragment size */
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
The fields correspond to those of the same name returned by
<B>statfs</B>().

<P>

<DT><B>fsinfo_attr_fsinfo</B>

<DD>
This retrieves information about the
<B>fsinfo</B>()

system call itself.  This fills in the following structure:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_fsinfo {
    __u32 max_attr;
    __u32 max_cap;
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
The
<I>max_attr</I>

value indicates the number of attributes supported by the
<B>fsinfo</B>()

system call, and
<I>max_cap</I>

indicates the number of capability bits supported by the
<B>fsinfo_attr_capabilities</B>

attribute.  The first corresponds to
<I>fsinfo_attr__nr</I>

and the second to
<I>fsinfo_cap__nr</I>

in the header file.
<P>

<DT><B>fsinfo_attr_ids</B>

<DD>
This retrieves a number of fixed IDs and other static information otherwise
available through
<B>statfs</B>().

The following structure is filled in:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_ids {
    char  f_fs_name[15 + 1]; /* Filesystem name */
    __u64 f_flags;      /* Filesystem mount flags (MS_*) */
    __u64 f_fsid;       /* Short 64-bit Filesystem ID */
    __u64 f_sb_id;      /* Internal superblock ID */
    __u32 f_fstype;     /* Filesystem type from linux/magic.h */
    __u32 f_dev_major;  /* As st_dev_* from struct statx */
    __u32 f_dev_minor;
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
Most of these are filled in as for
<B>statfs</B>(),

with the addition of the filesystem's symbolic name in
<I>f_fs_name</I>

and an identifier for use in notifications in
<I>f_sb_id</I>.

<P>

<DT><B>fsinfo_attr_limits</B>

<DD>
This retrieves information about the limits of what a filesystem can support.
The following structure is filled in:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_limits {
    __u64 max_file_size;
    __u64 max_uid;
    __u64 max_gid;
    __u64 max_projid;
    __u32 max_dev_major;
    __u32 max_dev_minor;
    __u32 max_hard_links;
    __u32 max_xattr_body_len;
    __u16 max_xattr_name_len;
    __u16 max_filename_len;
    __u16 max_symlink_len;
    __u16 __reserved[1];
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
These indicate the maximum supported sizes for a variety of filesystem objects,
including the file size, the extended attribute name length and body length,
the filename length and the symlink body length.
<DT><DD>
It also indicates the maximum representable values for a User ID, a Group ID,
a Project ID, a device major number and a device minor number.
<DT><DD>
And finally, it indicates the maximum number of hard links that can be made to
a file.
<DT><DD>
Note that some of these values may be zero if the underlying object or concept
is not supported by the filesystem or the medium.
<P>

<DT><B>fsinfo_attr_supports</B>

<DD>
This retrieves information about what bits a filesystem supports in various
masks.  The following structure is filled in:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_supports {
    __u64 stx_attributes;
    __u32 stx_mask;
    __u32 ioc_flags;
    __u32 win_file_attrs;
    __u32 __reserved[1];
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
The
<I>stx_attributes</I> and <I>stx_mask</I>

fields indicate what bits in the struct statx fields of the matching names
are supported by the filesystem.
<DT><DD>
The
<I>ioc_flags</I>

field indicates what FS_*_FL flag bits as used through the FS_IOC_GET/SETFLAGS
ioctls are supported by the filesystem.
<DT><DD>
The
<I>win_file_attrs</I>

indicates what DOS/Windows file attributes a filesystem supports, if any.
<P>

<DT><B>fsinfo_attr_capabilities</B>

<DD>
This retrieves information about what features a filesystem supports as a
series of single bit indicators.  The following structure is filled in:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_capabilities {
    __u8 capabilities[(fsinfo_cap__nr + 7) / 8];
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
where the bit of interest can be found by:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
        p-&gt;capabilities[bit / 8] &amp; (1 &lt;&lt; (bit % 8)))
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
The bits are listed by
<I>enum fsinfo_capability</I>

and
<B>fsinfo_cap__nr</B>

is one more than the last capability bit listed in the header file.
<DT><DD>
Note that the number of capability bits actually supported by the kernel can be
found using the
<B>fsinfo_attr_fsinfo</B>

attribute.
<DT><DD>
The capability bits and their meanings are listed below in the &quot;THE
CAPABILITIES&quot; section.
<P>

<DT><B>fsinfo_attr_timestamp_info</B>

<DD>
This retrieves information about what timestamp resolution and scope is
supported by a filesystem for each of the file timestamps.  The following
structure is filled in:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_timestamp_info {
        __s64 minimum_timestamp;
        __s64 maximum_timestamp;
        __u16 atime_gran_mantissa;
        __u16 btime_gran_mantissa;
        __u16 ctime_gran_mantissa;
        __u16 mtime_gran_mantissa;
        __s8  atime_gran_exponent;
        __s8  btime_gran_exponent;
        __s8  ctime_gran_exponent;
        __s8  mtime_gran_exponent;
        __u32 __reserved[1];
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
where
<I>minimum_timestamp</I> and <I>maximum_timestamp</I>

are the limits on the timestamps that the filesystem supports and
<I>*time_gran_mantissa</I> and <I>*time_gran_exponent</I>

indicate the granularity of each timestamp in terms of seconds, using the
formula:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
mantissa * pow(10, exponent) Seconds
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
where exponent may be negative and the result may be a fraction of a second.
<DT><DD>
Four timestamps are detailed: <B>A</B>ccess time, <B>B</B>irth/creation time,
<B>C</B>hange time and <B>M</B>odification time.  Capability bits are defined
that specify whether each of these exist in the filesystem or not.
<DT><DD>
Note that the timestamp description may be approximated or inaccurate if the
file is actually remote or is the union of multiple objects.
<P>

<DT><B>fsinfo_attr_volume_id</B>

<DD>
This retrieves the system's superblock volume identifier as a variable-length
string.  This does not necessarily represent a value stored in the medium but
might be constructed on the fly.
<DT><DD>
For instance, for a block device this is the block device identifier
(eg. &quot;sdb2&quot;); for AFS this would be the numeric volume identifier.
<P>

<DT><B>fsinfo_attr_volume_uuid</B>

<DD>
This retrieves the volume UUID, if there is one, as a little-endian binary
UUID.  This fills in the following structure:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_volume_uuid {
    __u8 uuid[16];
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
<P>

<DT><B>fsinfo_attr_volume_name</B>

<DD>
This retrieves the filesystem's volume name as a variable-length string.  This
is expected to represent a name stored in the medium.
<DT><DD>
For a block device, this might be a label stored in the superblock.  For a
network filesystem, this might be a logical volume name of some sort.
<P>

</DL>
<P>

<B>fsinfo_attr_cell_name</B>

<BR>

<B>fsinfo_attr_domain_name</B>

<BR>

<DL COMPACT>
<DT><DD>
These two attributes are variable-length string attributes that may be used to
obtain information about network filesystems.  An AFS volume, for instance,
belongs to a named cell.  CIFS shares may belong to a domain.
<P>

<DT><B>fsinfo_attr_realm_name</B>

<DD>
This attribute is variable-length string that indicates the Kerberos realm that
a filesystem's authentication tokens should come from.
<P>

<DT><B>fsinfo_attr_server_name</B>

<DD>
This attribute is a multiple-value attribute that lists the names of the
servers that are backing a network filesystem.  Each value is a variable-length
string.  The values are enumerated by calling
<B>fsinfo</B>()

multiple times, incrementing
<I>params-&gt;Nth</I>

each time until an ENODATA error occurs, thereby indicating the end of the
list.
<P>

<DT><B>fsinfo_attr_server_address</B>

<DD>
This attribute is a multiple-instance, multiple-value attribute that lists the
addresses of the servers that are backing a network filesystem.  Each value is
a structure of the following type:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_server_address {
    struct __kernel_sockaddr_storage address;
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
Where the address may be AF_INET, AF_INET6, AF_RXRPC or any other type as
appropriate to the filesystem.
<DT><DD>
The values are enumerated by calling
<I>fsinfo</I>()

multiple times, incrementing
<I>params-&gt;Nth</I>

to step through the servers and
<I>params-&gt;Mth</I>

to step through the addresses of the Nth server each time until ENODATA errors
occur, thereby indicating either the end of a server's address list or the end
of the server list.
<DT><DD>
Barring the server list changing whilst being accessed, it is expected that the
<I>params-&gt;Nth</I>

will correspond to
<I>params-&gt;Nth</I>

for
<B>fsinfo_attr_server_name</B>.

<P>

<DT><B>fsinfo_attr_parameter</B>

<DD>
This attribute is a multiple-value attribute that lists the values of the mount
parameters for a filesystem as variable-length strings.
<DT><DD>
The parameters are enumerated by calling
<B>fsinfo</B>()

multiple times, incrementing
<I>params-&gt;Nth</I>

to step through them until error ENODATA is given.
<DT><DD>
Parameter strings are presented in a form akin to the way they're passed to the
context created by the
<B>fsopen</B>()

system call.  For example, straight text parameters will be rendered as
something like:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
&quot;o data=journal&quot;
&quot;o noquota&quot;
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
Where the initial &quot;word&quot; indicates the option form.
<P>

<DT><B>fsinfo_attr_source</B>

<DD>
This attribute is a multiple-value attribute that lists the mount sources for a
filesystem as variable-length strings.  Normally only one source will be
available, but the possibility of having more than one is allowed for.
<DT><DD>
The sources are enumerated by calling
<B>fsinfo</B>()

multiple times, incrementing
<I>params-&gt;Nth</I>

to step through them until error ENODATA is given.
<DT><DD>
Source strings are presented in a form akin to the way they're passed to the
context created by the
<B>fsopen</B>()

system call.  For example, they will be rendered as something like:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
&quot;s /dev/sda1&quot;
&quot;s example.com/pub/linux/&quot;
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
Where the initial &quot;word&quot; indicates the option form.
<P>

<DT><B>fsinfo_attr_name_encoding</B>

<DD>
This attribute is variable-length string that indicates the filename encoding
used by the filesystem.  The default is &quot;utf8&quot;.  Note that this may indicate a
non-8-bit encoding if that's what the underlying filesystem actually supports.
<P>

<DT><B>fsinfo_attr_name_codepage</B>

<DD>
This attribute is variable-length string that indicates the codepage used to
translate filenames from the filesystem to the system if this is applicable to
the filesystem.
<P>

<DT><B>fsinfo_attr_io_size</B>

<DD>
This retrieves information about the I/O sizes supported by the filesystem.
The following structure is filled in:
</DL>
<P>

<DL COMPACT><DT><DD>

<PRE>
struct fsinfo_io_size {
    __u32 block_size;
    __u32 max_single_read_size;
    __u32 max_single_write_size;
    __u32 best_read_size;
    __u32 best_write_size;
};
</PRE>


</DL>

<DL COMPACT>
<DT><DD>
Where
<I>block_size</I>

indicates the fundamental I/O block size of the filesystem as something
O_DIRECT read/write sizes must be a multiple of;
<I>max_single_write_size</I> and <I>max_single_write_size</I>

indicate the maximum sizes for individual unbuffered data transfer operations;
and
<I>best_read_size</I> and <I>best_write_size</I>

indicate the recommended I/O sizes.
<DT><DD>
Note that any of these may be zero if inapplicable or indeterminable.
<P>
<P>
<P>
</DL>
<A NAME="lbAH">&nbsp;</A>
<H2>THE CAPABILITIES</H2>

<P>

There are number of capability bits in a bit array that can be retrieved using
<B>fsinfo_attr_capabilities</B>.

These give information about features of the filesystem driver and the specific
filesystem.
<P>

<P>

<B>fsinfo_cap_is_kernel_fs</B>

<BR>

<B>fsinfo_cap_is_block_fs</B>

<BR>

<B>fsinfo_cap_is_flash_fs</B>

<BR>

<B>fsinfo_cap_is_network_fs</B>

<BR>

<B>fsinfo_cap_is_automounter_fs</B>

<DL COMPACT>
<DT><DD>
These indicate the primary type of the filesystem.
<B>kernel</B>

filesystems are special communication interfaces that substitute files for
system calls; examples include procfs and sysfs.
<B>block</B>

filesystems require a block device on which to operate; examples include ext4
and XFS.
<B>flash</B>

filesystems require an MTD device on which to operate; examples include JFFS2.
<B>network</B>

filesystems require access to the network and contact one or more servers;
examples include NFS and AFS.
<B>automounter</B>

filesystems are kernel special filesystems that host automount points and
triggers to dynamically create automount points.  Examples include autofs and
AFS's dynamic root.
<P>

<DT><B>fsinfo_cap_automounts</B>

<DD>
The filesystem may have automount points that can be triggered by pathwalk.
<P>

<DT><B>fsinfo_cap_adv_locks</B>

<DD>
The filesystem supports advisory file locks.  For a network filesystem, this
indicates that the advisory file locks are cross-client (and also between
server and its local filesystem on something like NFS).
<P>

<DT><B>fsinfo_cap_mand_locks</B>

<DD>
The filesystem supports mandatory file locks.  For a network filesystem, this
indicates that the mandatory file locks are cross-client (and also between
server and its local filesystem on something like NFS).
<P>

<DT><B>fsinfo_cap_leases</B>

<DD>
The filesystem supports leases.  For a network filesystem, this means that the
server will tell the client to clean up its state on a file before passing the
lease to another client.
<P>

</DL>
<P>

<B>fsinfo_cap_uids</B>

<BR>

<B>fsinfo_cap_gids</B>

<BR>

<B>fsinfo_cap_projids</B>

<DL COMPACT>
<DT><DD>
These indicate that the filesystem supports numeric user IDs, group IDs and
project IDs respectively.
<P>

</DL>
<P>

<B>fsinfo_cap_id_names</B>

<BR>

<B>fsinfo_cap_id_guids</B>

<DL COMPACT>
<DT><DD>
These indicate that the filesystem employs textual names and/or GUIDs as
identifiers.
<P>

<DT><B>fsinfo_cap_windows_attrs</B>

<DD>
Indicates that the filesystem supports some Windows FILE_* attributes.
<P>

</DL>
<P>

<B>fsinfo_cap_user_quotas</B>

<BR>

<B>fsinfo_cap_group_quotas</B>

<BR>

<B>fsinfo_cap_project_quotas</B>

<DL COMPACT>
<DT><DD>
These indicate that the filesystem supports quotas for users, groups and
projects respectively.
<P>

</DL>
<P>

<B>fsinfo_cap_xattrs</B>

<BR>

<B>fsinfo_cap_symlinks</B>

<BR>

<B>fsinfo_cap_hard_links</B>

<BR>

<B>fsinfo_cap_hard_links_1dir</B>

<BR>

<B>fsinfo_cap_device_files</B>

<BR>

<B>fsinfo_cap_unix_specials</B>

<DL COMPACT>
<DT><DD>
These indicate that the filesystem supports respectively extended attributes;
symbolic links; hard links spanning direcories; hard links, but only within a
directory; block and character device files; and UNIX special files, such as
FIFO and socket.
<P>

</DL>
<P>

<B>fsinfo_cap_journal</B>

<BR>

<B>fsinfo_cap_data_is_journalled</B>

<DL COMPACT>
<DT><DD>
The first of these indicates that the filesystem has a journal and the second
that the file data changes are being journalled.
<P>

</DL>
<P>

<B>fsinfo_cap_o_sync</B>

<BR>

<B>fsinfo_cap_o_direct</B>

<DL COMPACT>
<DT><DD>
These indicate that O_SYNC and O_DIRECT are supported respectively.
<P>

</DL>
<P>

<B>fsinfo_cap_volume_id</B>

<BR>

<B>fsinfo_cap_volume_uuid</B>

<BR>

<B>fsinfo_cap_volume_name</B>

<BR>

<B>fsinfo_cap_volume_fsid</B>

<BR>

<B>fsinfo_cap_cell_name</B>

<BR>

<B>fsinfo_cap_domain_name</B>

<BR>

<B>fsinfo_cap_realm_name</B>

<DL COMPACT>
<DT><DD>
These indicate if various attributes are supported by the filesystem, where
<B>fsinfo_cap_X</B>

here corresponds to
<B>fsinfo_attr_X</B>.

<P>

</DL>
<P>

<B>fsinfo_cap_iver_all_change</B>

<BR>

<B>fsinfo_cap_iver_data_change</B>

<BR>

<B>fsinfo_cap_iver_mono_incr</B>

<DL COMPACT>
<DT><DD>
These indicate if
<I>i_version</I>

on an inode in the filesystem is supported and
how it behaves.
<B>all_change</B>

indicates that i_version is incremented on metadata changes as well as data
changes.
<B>data_change</B>

indicates that i_version is only incremented on data changes, including
truncation.
<B>mono_incr</B>

indicates that i_version is incremented by exactly 1 for each change made.
<P>

<DT><B>fsinfo_cap_resource_forks</B>

<DD>
This indicates that the filesystem supports some sort of resource fork or
alternate data stream on a file.  This isn't the same as an extended attribute.
<P>

</DL>
<P>

<B>fsinfo_cap_name_case_indep</B>

<BR>

<B>fsinfo_cap_name_non_utf8</B>

<BR>

<B>fsinfo_cap_name_has_codepage</B>

<DL COMPACT>
<DT><DD>
These indicate certain facts about the filenames in a filesystem: whether
they're case-independent; if they're not UTF-8; and if there's a codepage
employed to map the names.
<P>

<DT><B>fsinfo_cap_sparse</B>

<DD>
This indicates that the filesystem supports sparse files.
<P>

<DT><B>fsinfo_cap_not_persistent</B>

<DD>
This indicates that the filesystem is not persistent, and that any data stored
here will not be saved in the event that the filesystem is unmounted, the
machine is rebooted or the machine loses power.
<P>

<DT><B>fsinfo_cap_no_unix_mode</B>

<DD>
This indicates that the filesystem doesn't support the UNIX mode permissions
bits.
<P>

</DL>
<P>

<B>fsinfo_cap_has_atime</B>

<BR>

<B>fsinfo_cap_has_btime</B>

<BR>

<B>fsinfo_cap_has_ctime</B>

<BR>

<B>fsinfo_cap_has_mtime</B>

<DL COMPACT>
<DT><DD>
These indicate as to what timestamps a filesystem supports, including: Access
time, Birth/creation time, Change time (metadata and data) and Modification
time (data only).
<P>
<P>



</DL>
<A NAME="lbAI">&nbsp;</A>
<H2>RETURN VALUE</H2>

On success, the size of the value that the kernel has available is returned,
irrespective of whether the buffer is large enough to hold that.  The data
written to the buffer will be truncated if it is not.  On error, -1 is
returned, and
<I>errno</I>

is set appropriately.
<A NAME="lbAJ">&nbsp;</A>
<H2>ERRORS</H2>

<DL COMPACT>
<DT><B>EACCES</B>

<DD>
Search permission is denied for one of the directories
in the path prefix of
<I>pathname</I>.

(See also
<B><A HREF="/man/man2html?7+path_resolution">path_resolution</A></B>(7).)

<DT><B>EBADF</B>

<DD>
<I>dirfd</I>

is not a valid open file descriptor.
<DT><B>EFAULT</B>

<DD>
<I>pathname</I>

is NULL or
<I>pathname</I>, <I>params</I> or <I>buffer</I>

point to a location outside the process's accessible address space.
<DT><B>EINVAL</B>

<DD>
Reserved flag specified in
<I>params-&gt;at_flags</I> or one of <I>params-&gt;__reserved[]</I>

is not 0.
<DT><B>EOPNOTSUPP</B>

<DD>
Unsupported attribute requested in
<I>params-&gt;request</I>.

This may be beyond the limit of the supported attribute set or may just not be
one that's supported by the filesystem.
<DT><B>ENODATA</B>

<DD>
Unavailable attribute value requested by
<I>params-&gt;Nth</I> and/or <I>params-&gt;Mth</I>.

<DT><B>ELOOP</B>

<DD>
Too many symbolic links encountered while traversing the pathname.
<DT><B>ENAMETOOLONG</B>

<DD>
<I>pathname</I>

is too long.
<DT><B>ENOENT</B>

<DD>
A component of
<I>pathname</I>

does not exist, or
<I>pathname</I>

is an empty string and
<B>AT_EMPTY_PATH</B>

was not specified in
<I>params-&gt;at_flags</I>.

<DT><B>ENOMEM</B>

<DD>
Out of memory (i.e., kernel memory).
<DT><B>ENOTDIR</B>

<DD>
A component of the path prefix of
<I>pathname</I>

is not a directory or
<I>pathname</I>

is relative and
<I>dirfd</I>

is a file descriptor referring to a file other than a directory.
</DL>
<A NAME="lbAK">&nbsp;</A>
<H2>VERSIONS</H2>

<B>fsinfo</B>()

was added to Linux in kernel 4.18.
<A NAME="lbAL">&nbsp;</A>
<H2>CONFORMING TO</H2>

<B>fsinfo</B>()

is Linux-specific.
<A NAME="lbAM">&nbsp;</A>
<H2>NOTES</H2>

Glibc does not (yet) provide a wrapper for the
<B>fsinfo</B>()

system call; call it using
<B><A HREF="/man/man2html?2+syscall">syscall</A></B>(2).

<A NAME="lbAN">&nbsp;</A>
<H2>SEE ALSO</H2>

<B><A HREF="/man/man2html?2+ioctl_iflags">ioctl_iflags</A></B>(2),

<B><A HREF="/man/man2html?2+statx">statx</A></B>(2),

<B><A HREF="/man/man2html?2+statfs">statfs</A></B>(2)

<P>

<HR>
<A NAME="index">&nbsp;</A><H2>Index</H2>
<DL>
<DT><A HREF="#lbAB">NAME</A><DD>
<DT><A HREF="#lbAC">SYNOPSIS</A><DD>
<DT><A HREF="#lbAD">DESCRIPTION</A><DD>
<DL>
<DT><A HREF="#lbAE">Allowance for Future Attribute Expansion</A><DD>
<DT><A HREF="#lbAF">Invoking <B>fsinfo</B>():</A><DD>
</DL>
<DT><A HREF="#lbAG">THE ATTRIBUTES</A><DD>
<DT><A HREF="#lbAH">THE CAPABILITIES</A><DD>
<DT><A HREF="#lbAI">RETURN VALUE</A><DD>
<DT><A HREF="#lbAJ">ERRORS</A><DD>
<DT><A HREF="#lbAK">VERSIONS</A><DD>
<DT><A HREF="#lbAL">CONFORMING TO</A><DD>
<DT><A HREF="#lbAM">NOTES</A><DD>
<DT><A HREF="#lbAN">SEE ALSO</A><DD>
</DL>
<HR>
This document was created by
<A HREF="/man/man2html">man2html</A>,
using the manual pages.<BR>
Time: 21:25:28 GMT, June 18, 2018
</BODY>
</HTML>
