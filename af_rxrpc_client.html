<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
  <STYLE type="text/css">
    CODE.example { color: blue }
  </STYLE>
  <HEAD>
    <TITLE>AF_RXRPC: Client</TITLE>
  </HEAD>
  <BODY>
    <HEADER>
    </HEADER>
    <MAIN>
      <H1>Example userspace AF_RXRPC client usage</H1>
      <TEXT>
	<P>
	  AF_RXRPC is implemented as a network protocol and, as such, is
	  accessible from userspace through the Linux socket interface.  A
	  socket is opened and then multiple calls may be made over it using
	  the ancillary data provided to <CODE>sendmsg()</CODE>
	  and <CODE>recvmsg()</CODE> to determine the call.
	</P>
	<H2>Prologue</H2>
	<P>
	  First of all, an AF_RXRPC socket must be opened:
	</P>
	<CODE class="example"><PRE>
            client = socket(AF_RXRPC, SOCK_DGRAM, PF_INET);</PRE></CODE>
	<P>
	  The second argument is the transport socket type (which must
	  be <CODE>DGRAM</CODE> for the moment) and the third argument
	  specified the transport protocol (<CODE>PF_INET</CODE>
	  or <CODE>PF_INET6</CODE>).  This gets you a file descriptor that can
	  be used to perform RPC message passing.  You can then set the
	  security on it:
	</P>
	<CODE class="example"><PRE>
            int sec = RXRPC_SECURITY_AUTH;
            ret = setsockopt(client, SOL_RXRPC, RXRPC_MIN_SECURITY_LEVEL, &amp;sec, sizeof(sec));
            const char principal[] = "afs@GRAND.CENTRAL.ORG";
            ret = setsockopt(client, SOL_RXRPC, RXRPC_SECURITY_KEY, principal, strlen(principal));</PRE></CODE>
	<P>
	  The first statement sets the minimum security level required to one
	  of checksum only (<CODE>PLAIN</CODE>), authenticated packet only
	  (<CODE>AUTH</CODE>) or full packet encryption (<CODE>ENCRYPT</CODE>).
	  The second statement sets the name of the key to be used.
	<P>
	  An address can then be bound to the local endpoint.  This is optional
	  for a client, but if a specific address and/or port is required for
	  the local UDP transport, then this must be used.
	</P>
	<CODE class="example"><PRE>
            memset(&amp;my_addr, 0, sizeof(my_addr));
            my_addr.my_addr_family              = AF_RXRPC;
            my_addr.my_addr_service             = 0;
            my_addr.transport_type              = SOCK_DGRAM;
            my_addr.transport_len               = sizeof(my_addr.transport.sin);
            my_addr.transport.sin.sin_family    = AF_INET;
            my_addr.transport.sin.sin_port      = htons(7001);
            memcpy(&amp;my_addr.transport.sin.sin_addr, &amp;local_addr, 4);

            ret = bind(client, (struct sockaddr *)&amp;my_addr, sizeof(my_addr));</PRE></CODE>
	<P>
	  For a service, <CODE>.srx_service</CODE> is where the service ID on
	  which the caller wishes to listen is specified; this should
	  be <CODE>0</CODE> for a purely client socket.
	<P>
	  It should be noted that client sockets opened separately may share a
	  UDP port inside the kernel with each other, but sharing a UDP socket
	  between a service and a client or between a pair of services is
	  forbidden.
	<P>
	  We then need to set up the address of the server to which we're going
	  to try to talk:
	</P>
	<CODE class="example"><PRE>
            peer_addr.peer_addr_family          = AF_RXRPC;
            peer_addr.peer_addr_service         = 52;
            peer_addr.transport_type            = SOCK_DGRAM;
            peer_addr.transport_len             = sizeof(peer_addr.transport.sin);
            peer_addr.transport.sin.sin_family  = AF_INET;
            peer_addr.transport.sin.sin_port    = htons(7003);
            memcpy(&amp;peer_addr.transport.sin.sin_addr, &amp;remote_addr, 4);</PRE></CODE>
	<P>
	  The <CODE>.srx_service</CODE> member holds the service ID that we
	  wish to contact on the far port. <CODE>.transport</CODE> holds the
	  transport socket parameters.  The <CODE>.transport_type</CODE>
	  and <CODE>.transport_family</CODE> must match the values given to
	  socket as <CODE>type</CODE>
	  and <CODE>protocol</CODE>.  <CODE>.transport.sin</CODE> carries the
	  target UDP address.
	</P>
	<H2>Call Initiation and Transmission Phase</H2>
	<P>
	  Next we need to load up the ancillary data into the control buffer
	  for <CODE>sendmsg()</CODE> to use in call multiplexing:
	</P>
	<CODE class="example"><PRE>
            ctrllen = 0;
            RXRPC_ADD_CALLID(control, ctrllen, 0x12345);</PRE></CODE>
	<P>
	  This includes userspace's identifier for the call we're about to
	  launch (in this case <CODE>0x12345</CODE>).  It must be included in
	  all subsequent <CODE>sendmsg()</CODE> calls and it will be included
	  in the return from all subsequent <CODE>recvmsg()</CODE> calls.  It
	  is valid until <CODE>recvmsg()</CODE> returns <CODE>MSG_EOR</CODE> in
	  conjuction with this call ID.  The call identifier is an integer of
	  size <CODE>unsigned long</CODE> and may be used to hold a pointer if
	  that is convenient.
	<P>
	  We then marshal the request data into a buffer:
	</P>
	<CODE class="example"><PRE>
            static char vlname[] = "root.cell";
            vllen = sizeof(vlname) - 1;
            param[0] = htonl(VLGETENTRYBYNAME);
            param[1] = htonl(vllen);
            padding = 0;</PRE></CODE>
	<P>
	  And then we call sendmsg() to send the data, inserting suitable
	  padding to round strings up to four-byte alignment:
	</P>
	<CODE class="example"><PRE>
            iov[0].iov_len  = sizeof(param);
            iov[0].iov_base = param;
            iov[1].iov_len  = vllen;
            iov[1].iov_base = vlname;
            iov[2].iov_len  = 4 - (vllen &amp; 3);
            iov[2].iov_base = &amp;padding;
            ioc = (iov[2].iov_len > 0) ? 3 : 2;

            msg.msg_name            = (struct sockaddr *)&amp;peer_addr;
            msg.msg_namelen         = sizeof(peer_addr);
            msg.msg_iov             = iov;
            msg.msg_iovlen          = ioc;
            msg.msg_control         = control;
            msg.msg_controllen      = ctrllen;
            msg.msg_flags           = 0;

            ret = sendmsg(client, &amp;msg, 0);</PRE></CODE>
	<P>
	  Note that we pass MSG_MORE if we want to send more data.  AF_RXRPC
	  will collect the data into appropriately sized packets so that there
	  is no wastage of sequence number space (there is a limit to the
	  number of DATA packets that can be sent in each direction).
	</P>
	<H2>Reception Phase</H2>
	<P>
	  We can listen for a response from the server:
	</P>
	<CODE class="example"><PRE>
            iov[0].iov_base = buffer;
            iov[0].iov_len = sizeof(buffer);

            msg.msg_name            = &amp;rx_addr;
            msg.msg_namelen         = sizeof(rx_addr);
            msg.msg_iov             = iov;
            msg.msg_iovlen          = 1;
            msg.msg_control         = control;
            msg.msg_controllen      = sizeof(control);
            msg.msg_flags           = 0;

            ret = recvmsg(client, &amp;msg, 0);</PRE></CODE>
	<P>
	  This will return <CODE>0</CODE> or the number of bytes read into the
	  buffer if it successfully queried a call - even if that call failed -
	  and <CODE>-ENODATA</CODE> if no calls were outstanding on a client
	  socket.  If a call did fail, the control message buffer will hold
	  ancillary data indicating the reason.  The ancillary data will also
	  hold the call identifier.  It should be noted that if several calls
	  are in progress simultaneously upon a socket, consecutive calls
	  to <CODE>recvmsg()</CODE> may return information from different
	  calls.  It is possible, however, to specify <CODE>MSG_PEEK</CODE> to
	  find out what the next call will be without altering the call state.
	<P>
	  If there is more data to be read because the call has not yet reached
	  the end of its reception phase, then <CODE>MSG_MORE</CODE> will be
	  returned.
	<P>
	  If any terminal indication is given, then the recvmsg will have
	  indicated <CODE>MSG_EOR</CODE> to indicate that the call identifier
	  is now released and can be reused.
	<P>
	  The contents of the control buffer then need to be parsed:
	</P>
	<CODE class="example"><PRE>
            struct cmsghdr *cmsg;
            unsigned long call_identifier;
            int abort_code, error;
            enum {
                call_ongoing,
                call_remotely_aborted,
                service_call_finally_acked,
                call_network_error,
                call_error,
                call_busy,
            } call_state = call_ongoing;

            for (cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg, cmsg)) {
                    void *p = CMSG_DATA(cmsg);
                    int n = cmsg->cmsg_len - CMSG_ALIGN(sizeof(*cmsg));

                    if (cmsg->cmsg_level == SOL_RXRPC) {
                            switch (cmsg->cmsg_type) {
                            case RXRPC_USER_CALL_ID:
                                    memcpy(&amp;call_identifier, p, sizeof(user_id));
                                    continue;
                            case RXRPC_ABORT:
                                    memcpy(&amp;abort_code, p, sizeof(abort_code));
                                    call_state = call_remotely_aborted;
                                    continue;
                            case RXRPC_ACK:
                                    call_state = service_call_finally_acked;
                                    continue;
                            case RXRPC_NET_ERROR:
                                    memcpy(&amp;error, p, sizeof(error));
                                    errno = error;
                                    call_state = call_network_error;
                                    continue;
                            case RXRPC_BUSY:
                                    call_state = call_busy;
                                    continue;
                            case RXRPC_LOCAL_ERROR:
                                    memcpy(&amp;error, p, sizeof(error));
                                    errno = error;
                                    call_state = call_error;
                                    continue;
                            default:
                                    break;
                            }
                    }
            }</CODE></PRE>
	<P>
	  The above snippet parses the control buffer, extracting the call
	  identifier along with the code of any abort received and any local or
	  network error incurred.  It also extracts the indication that the
	  call was terminated because the server kept returning a busy packet
	  and the indication that a service call received the final ACK from
	  the remote client.
	</P>
	<H2>Aborting a Call</H2>
	<P>
	  An active call may be aborted as long as recvmsg() hasn't yet
	  indicated that the call has been terminated by
	  flagging <CODE>MSG_EOR</CODE>.  This is done by specifying the call
	  identifier and the abort code in the control message:
	</P>
	<CODE class="example"><PRE>
            ctrllen = 0;
            RXRPC_ADD_CALLID(control, ctrllen, 0x12345);
            RXRPC_ADD_ABORT(control, ctrllen, 0x6789);

            msg.msg_name            = NULL;
            msg.msg_namelen         = 0;
            msg.msg_iov             = NULL;
            msg.msg_iovlen          = 0;
            msg.msg_control         = control;
            msg.msg_controllen      = ctrllen;
            msg.msg_flags           = 0;

            ret = sendmsg(client, &amp;msg, 0);</PRE></CODE>
	<P>
	  Note that this still has to be followed with a call
	  to <CODE>recvmsg()</CODE> to find out how the call actually ended
	  (it's possible the call was aborted first from the other side, for
	  example).
	</P>
      </TEXT>
    </MAIN>
  </BODY>
</HTML>
